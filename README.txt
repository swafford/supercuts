Developed and tested in Windows 7, Python 2.7.
Contact Swafford.Andrew@lifesci.ucsb.edu with questions/comments
Last edited: April 25th 2016

Purpose:  The purpose of supercuts is to remove sequences from an alignment file that do not appear on a Tree.
Ideally, the alignment will be used to create an initial tree, then removal of unwanted sequences can be done
by hand, and supercuts can be used to remove the sequences from the alignment / original dataset. OTU labels
and sequence names must be identical.

usage: supercuts.py [-h] -p prefix [-o output] -t treeFile -a alnFile

Cut alignment to match a tree

optional arguments:
  -h, --help   show this help message and exit
  -p prefix    Prefix to append onto output files
  -o output    Path to output dir.
  -t treeFile  Path to tree file (NEWICK).
  -a alnFile   Path to alignment file (FASTA).